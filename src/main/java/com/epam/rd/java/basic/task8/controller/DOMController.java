package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.ObjectFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	DocumentBuilder documentBuilder;
	List<Flowers.Flower> flowersList = new ArrayList<>();
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			documentBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	Document document = null ;

	public Flowers buildFlowers() {
		try {
			document = documentBuilder.parse(xmlFileName);
			Element root = document.getDocumentElement();
			NodeList flowersList = root.getElementsByTagName("flower");
			for (int i = 0; i < flowersList.getLength(); i++) {
				Element flowerElement = (Element) flowersList.item(i);
				Flowers.Flower flower = buildFlower(flowerElement);
				this.flowersList.add(flower);
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		Flowers flowers = new Flowers();
		flowers.getFlower().addAll(flowersList);
		return flowers;
	}

	private Flowers.Flower buildFlower(Element flowerElement){
		Flowers.Flower flower = new ObjectFactory().createFlowersFlower();
		flower.setName(getElementTextContent(flowerElement, "name"));
		flower.setSoil(getElementTextContent(flowerElement, "soil"));
		flower.setOrigin(getElementTextContent(flowerElement, "origin"));
///////////////////////////////////////////////////
////visual
		Flowers.Flower.VisualParameters visualParameters = new ObjectFactory().createFlowersFlowerVisualParameters();
		Element visualParametersElement = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
		visualParameters.setStemColour(getElementTextContent(visualParametersElement, "stemColour"));
		visualParameters.setLeafColour(getElementTextContent(visualParametersElement, "leafColour"));


		Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new ObjectFactory()
				.createFlowersFlowerVisualParametersAveLenFlower();
		NodeList aveLenNodeList =  visualParametersElement.getElementsByTagName("aveLenFlower");
		String measure = aveLenNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
		BigInteger value = BigInteger.valueOf(Integer.parseInt(aveLenNodeList.item(0).getTextContent()));
		aveLenFlower.setMeasure(measure);
		aveLenFlower.setValue(value);
		visualParameters.setAveLenFlower(aveLenFlower);
		flower.setVisualParameters(visualParameters);
///////////////////////////////////////////////////
////growing
		Flowers.Flower.GrowingTips growingTips = new ObjectFactory().createFlowersFlowerGrowingTips();
		Element growingTipsElement = (Element) flowerElement.getElementsByTagName("growingTips").item(0);

		Flowers.Flower.GrowingTips.Tempreture tempreture = new ObjectFactory().createFlowersFlowerGrowingTipsTempreture();
		NodeList tempretureNodeList = growingTipsElement.getElementsByTagName("tempreture");
		String measuret = tempretureNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
		BigInteger valuet = BigInteger.valueOf(Integer.parseInt(tempretureNodeList.item(0).getTextContent()));
		tempreture.setMeasure(measuret);
		tempreture.setValue(valuet);
		growingTips.setTempreture(tempreture);

		Flowers.Flower.GrowingTips.Lighting lighting = new ObjectFactory().createFlowersFlowerGrowingTipsLighting();
		NodeList lightingNodeList = growingTipsElement.getElementsByTagName("lighting");
		String lightingL = lightingNodeList.item(0).getAttributes().getNamedItem("lightRequiring").getTextContent();
		lighting.setLightRequiring(lightingL);
		growingTips.setLighting(lighting);

		Flowers.Flower.GrowingTips.Watering watering = new ObjectFactory().createFlowersFlowerGrowingTipsWatering();
		NodeList wateringNodeList = growingTipsElement.getElementsByTagName("watering");
		String measurew = wateringNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
		BigInteger valuew = BigInteger.valueOf(Integer.parseInt(wateringNodeList.item(0).getTextContent()));
		watering.setMeasure(measurew);
		watering.setValue(valuew);
		growingTips.setWatering(watering);
		flower.setGrowingTips(growingTips);
///////////////////////////////////////////////////

		flower.setMultiplying(getElementTextContent(flowerElement, "multiplying"));
		return flower;
	}

	private static String getElementTextContent(Element element, String elementName){
		NodeList nd = element.getElementsByTagName(elementName);
		Node node = nd.item(0);
		return node.getTextContent();
	}


}
