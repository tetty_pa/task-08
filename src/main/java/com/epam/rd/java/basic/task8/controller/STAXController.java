package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.FlowerEnum;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final List<Flowers.Flower> flowerList = new ArrayList<>();

	private Flowers.Flower flower ;
	private Flowers.Flower.VisualParameters visualParameters;
	private Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower;

	private Flowers.Flower.GrowingTips growingTips;
	private Flowers.Flower.GrowingTips.Tempreture tempreture;
	private Flowers.Flower.GrowingTips.Lighting lighting;
	private Flowers.Flower.GrowingTips.Watering watering;
	private FlowerEnum currentEnum = null;
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers buildFlowers() throws XMLStreamException {
		String currentElement = null;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

	    while(reader.hasNext()){
			XMLEvent event = reader.nextEvent();

			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			if(event.isStartElement()){
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if(FlowerEnum.FLOWER.getValue().equals(currentElement)){
					flower = new Flowers.Flower();
					continue;
				}if(FlowerEnum.VISUALPARAMETERS.getValue().equals(currentElement)){
					visualParameters = new Flowers.Flower.VisualParameters();
					continue;
				}if("aveLenFlower".equals(currentElement) ){
					aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
					Attribute measure = startElement.getAttributeByName(new QName("measure"));
					aveLenFlower.setMeasure(measure.getValue());
					currentEnum = FlowerEnum.AVE_VALUE;
					continue;
				}if("growingTips".equals(currentElement)){
					growingTips = new Flowers.Flower.GrowingTips();
					continue;
				}if("tempreture".equals(currentElement)){
					tempreture = new Flowers.Flower.GrowingTips.Tempreture();
					Attribute measure = startElement.getAttributeByName(new QName("measure"));
					tempreture.setMeasure(measure.getValue());
					currentEnum = FlowerEnum.TEM_VALUE;
					continue;
				}if("lighting".equals(currentElement)){
					lighting = new Flowers.Flower.GrowingTips.Lighting();
					Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
					lighting.setLightRequiring(lightRequiring.getValue());
					continue;
				}if("watering".equals(currentElement)){
					watering = new Flowers.Flower.GrowingTips.Watering();
					Attribute measure = startElement.getAttributeByName(new QName("measure"));
					watering.setMeasure(measure.getValue());
					currentEnum = FlowerEnum.WAT_VALUE;
				}
			}

			if(event.isCharacters()){
				Characters characters = event.asCharacters();
				String elementText = characters.getData();

				if(FlowerEnum.NAME.getValue().equals(currentElement)){
					flower.setName(elementText);
					continue;
				}
				if(FlowerEnum.SOIL.getValue().equals(currentElement)){
					flower.setSoil(elementText);
					continue;
				}
				if(FlowerEnum.ORIGIN.getValue().equals(currentElement)){
					flower.setOrigin(elementText);
					continue;
				}
				if(FlowerEnum.STEMCOLOUR.getValue().equals(currentElement)){
					visualParameters.setStemColour(elementText);
					continue;
				}
				if(FlowerEnum.LEAFCOLOUR.getValue().equals(currentElement)){
					visualParameters.setLeafColour(elementText);
					continue;
				}
				if(FlowerEnum.TEM_VALUE.getValue().equals(currentElement)){
					tempreture.setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
					continue;
				}
				if(FlowerEnum.WAT_VALUE.equals(currentEnum)){
					watering.setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
					currentEnum = null;
					continue;
				}
				if(FlowerEnum.AVE_VALUE.equals(currentEnum)){
					aveLenFlower.setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
					currentEnum = null;
					continue;
				}
				if(FlowerEnum.TEM_VALUE.equals(currentEnum)){
					tempreture.setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
					currentEnum = null;
					continue;
				}
				if(FlowerEnum.LIGHTING_REQUIRING.getValue().equals(currentElement)){
					lighting.setLightRequiring(elementText);
					continue;
				}
				if(FlowerEnum.WAT_VALUE.getValue().equals(currentElement)){
					watering.setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
					continue;
				}
				if(FlowerEnum.MULTIPLYING.getValue().equals(currentElement)){
					flower.setMultiplying(elementText);
				}
			}

			if(event.isEndElement()){
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();
				if("flower".equals(localName)){
					flowerList.add(flower);
				}
				if("visualParameters".equals(localName)){
					flower.setVisualParameters(visualParameters);
				}
				if("aveLenFlower".equals(localName)) {
					visualParameters.setAveLenFlower(aveLenFlower);
				}
				if("growingTips".equals(localName)) {
					flower.setGrowingTips(growingTips);
				}
				if("tempreture".equals(localName)){
					growingTips.setTempreture(tempreture);
				}
				if("lighting".equals(localName)){
					growingTips.setLighting(lighting);
				}
				if("watering".equals(localName)){
					growingTips.setWatering(watering);
				}

			}
		}
		reader.close();
		Flowers flowers = new Flowers();
		flowers.getFlower().addAll(flowerList);
		return flowers;
	}

}