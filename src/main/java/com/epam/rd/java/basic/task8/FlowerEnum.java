package com.epam.rd.java.basic.task8;

public enum FlowerEnum {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    AVE_MEASURE("measure"),
    AVE_VALUE("value"),
    VISUALPARAMETERS("visualParameters"),
    TEMPRETURE("tempreture"),
    TEM_MEASURE("measure"),
    TEM_VALUE("value"),
    LIGHTING_REQUIRING("lightingRequiring"),
    LIGHTING("lighting"),
    WATERING("watering"),
    WAT_MEASURE("measure"),
    WAT_VALUE("value"),
    GROWINGTIPS("growingTips"),
    MULTIPLYING("multiplying");
    private String value;

    private FlowerEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
