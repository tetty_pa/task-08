package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.FlowerEnum;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
    private final String xmlFileName;
    private final SAXHandler saxHandler = new SAXHandler();
    private XMLReader xmlReader;
    private List<Flowers.Flower> flowersList;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            xmlReader = saxParser.getXMLReader();
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        xmlReader.setContentHandler(saxHandler);
    }


    public Flowers buildFlowers() {
        try {
            xmlReader.parse(xmlFileName);
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
        flowersList = saxHandler.getFlowers();
        Flowers flowers = new Flowers();
        flowers.getFlower().addAll(flowersList);
        return flowers;
    }

}

class SAXHandler extends DefaultHandler {
    private final List<Flowers.Flower> flowers;
    private Flowers.Flower flower;
    private Flowers.Flower.VisualParameters visualParameters;
    private Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower;

    private Flowers.Flower.GrowingTips growingTips;
    private Flowers.Flower.GrowingTips.Tempreture tempreture;
    private Flowers.Flower.GrowingTips.Lighting lighting;
    private Flowers.Flower.GrowingTips.Watering watering;

    private FlowerEnum currentEnum = null;
    private final EnumSet<FlowerEnum> withText;

    public SAXHandler() {
        flowers = new ArrayList<>();
        withText = EnumSet.range(FlowerEnum.NAME, FlowerEnum.MULTIPLYING);
    }

    public List<Flowers.Flower> getFlowers() {
        return flowers;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if ("flower".equals(qName)) {
            flower = new Flowers.Flower();
        } else if ("visualParameters".equals(qName)) {
            visualParameters = new Flowers.Flower.VisualParameters();
        } else if ("aveLenFlower".equals(qName)) {
            aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
            aveLenFlower.setMeasure(attributes.getValue("measure"));
            currentEnum = FlowerEnum.AVE_VALUE;
        } else if ("growingTips".equals(qName)) {
            growingTips = new Flowers.Flower.GrowingTips();
        } else if ("tempreture".equals(qName)) {
            tempreture = new Flowers.Flower.GrowingTips.Tempreture();
            tempreture.setMeasure(attributes.getValue("measure"));
            currentEnum = FlowerEnum.TEM_VALUE;
        } else if ("lighting".equals(qName)) {
            lighting = new Flowers.Flower.GrowingTips.Lighting();
            lighting.setLightRequiring(attributes.getValue("lightRequiring"));
        } else if ("watering".equals(qName)) {
            watering = new Flowers.Flower.GrowingTips.Watering();
            watering.setMeasure(attributes.getValue("measure"));
            currentEnum = FlowerEnum.WAT_VALUE;
        } else {
            FlowerEnum temp = FlowerEnum.valueOf(qName.toUpperCase());
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ("flower".equals(qName)) {
            flowers.add(flower);
        }
        if ("visualParameters".equals(qName)) {
            flower.setVisualParameters(visualParameters);
        }
        if ("aveLenFlower".equals(qName)) {
            visualParameters.setAveLenFlower(aveLenFlower);
        }
        if ("growingTips".equals(qName)) {
            flower.setGrowingTips(growingTips);
        }
        if ("tempreture".equals(qName)) {
            growingTips.setTempreture(tempreture);
        }
        if ("lighting".equals(qName)) {
            growingTips.setLighting(lighting);
        }
        if ("watering".equals(qName)) {
            growingTips.setWatering(watering);
        }

    }


    @Override
    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    flower.setName(s);
                    break;
                case SOIL:
                    flower.setSoil(s);
                    break;
                case ORIGIN:
                    flower.setOrigin(s);
                    break;

                case STEMCOLOUR:
                    visualParameters.setStemColour(s);
                    break;
                case LEAFCOLOUR:
                    visualParameters.setLeafColour(s);
                    break;
                case AVE_VALUE:
                    aveLenFlower.setValue(BigInteger.valueOf(Integer.parseInt(s)));
                    break;

                case TEM_VALUE:
                    tempreture.setValue(BigInteger.valueOf(Integer.parseInt(s)));
                    break;
                case LIGHTING_REQUIRING:
                    lighting.setLightRequiring(s);
                    break;
                case WAT_VALUE:
                    watering.setValue(BigInteger.valueOf(Integer.parseInt(s)));
                    break;
                case MULTIPLYING:
                    flower.setMultiplying(s);
                    break;
                //default -> throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}





