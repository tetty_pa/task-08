
@XmlSchema(
        namespace = "http://www.nure.ua",
        elementFormDefault = XmlNsForm.QUALIFIED
)
        package com.epam.rd.java.basic.task8;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;