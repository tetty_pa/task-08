package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers domFlowers = domController.buildFlowers();

		// sort (case 1)
		//domFlowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		Jaxb m = new Jaxb(outputXmlFile, domFlowers);
		m.saveToXML();

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		Flowers saxFlowers = saxController.buildFlowers();

		// sort  (case 2)
		saxFlowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getMultiplying));

		// save

		outputXmlFile = "output.sax.xml";
		m = new Jaxb(outputXmlFile, saxFlowers);
		m.saveToXML();


		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		Flowers staxFlowers = staxController.buildFlowers();

		// sort  (case 3)
		staxFlowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getSoil));
		
		// save
		outputXmlFile = "output.stax.xml";
		m = new Jaxb(outputXmlFile, staxFlowers);
		m.saveToXML();
	}


}

