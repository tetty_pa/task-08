package com.epam.rd.java.basic.task8;

import javax.xml.bind.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Jaxb {
    String file;
    Flowers flowers;

    public Jaxb(String file, Flowers flowers) {
        this.file = file;
        this.flowers = flowers;
    }

    public void saveToXML() throws JAXBException, FileNotFoundException {
        JAXBContext jc = JAXBContext.newInstance(Flowers.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.nure.ua input.xsd");
        marshaller.marshal(flowers, new FileOutputStream(file ));
    }

}
